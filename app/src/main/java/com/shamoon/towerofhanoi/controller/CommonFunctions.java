package com.shamoon.towerofhanoi.controller;

import android.util.Log;

public class CommonFunctions {
    public static void logErrorMessage(String message){
        Log.d("error",message);
    }

    public static void logErrorMessage(String tag, String message){
        Log.d(tag, message);
    }
}
