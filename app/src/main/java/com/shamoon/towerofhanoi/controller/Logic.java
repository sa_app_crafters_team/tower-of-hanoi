package com.shamoon.towerofhanoi.controller;

import java.io.PrintStream;

public class Logic {
    static int moves = 0;
    static int totalDisks = 0;

    public static int getMinimumMoves(int disks){
        int minimumMoves = 0;
        //solveHanoi(disks, fromPole, toPole, withPole, ps);
        return minimumMoves;
    }
    private static void solveHanoi(int disks, char fromPole, char toPole, char withPole, PrintStream ps){
        if (disks >= 1) {
            solveHanoi(disks-1, fromPole, withPole, toPole, ps);
            moveDisk(fromPole, toPole, ps);
            solveHanoi(disks-1, withPole, toPole, fromPole, ps);
        }
    }
    private static void moveDisk(char fromPole, char toPole, PrintStream ps) {
        moves++;
    }


}
