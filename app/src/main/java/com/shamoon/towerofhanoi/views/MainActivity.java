package com.shamoon.towerofhanoi.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.firestore.DocumentReference;
import com.shamoon.towerofhanoi.R;
import com.shamoon.towerofhanoi.databinding.ActivityMainBinding;
import com.shamoon.towerofhanoi.model.User;

import java.util.LinkedList;

import static com.shamoon.towerofhanoi.model.Constants.ROOM;
import static com.shamoon.towerofhanoi.model.Constants.USER;

public class MainActivity extends AppCompatActivity {

    GoogleSignInClient googleSignInClient;
    ActivityMainBinding binding;
    User user;
    String roomID;
    LinkedList<Integer> disc1;
    LinkedList<Integer> disc2;
    LinkedList<Integer> disc3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        init();

    }

    private void init() {
        user = getUserFromIntent();
        roomID = getRoomIDFromIntent();
        initGoogleSignInClient();
        setMessageToMessageTextView(user);
        disc1 = new LinkedList<>();
        disc2 = new LinkedList<>();
        disc3 = new LinkedList<>();
    }

    private User getUserFromIntent() {
        return (User) getIntent().getSerializableExtra(USER);
    }
    private String getRoomIDFromIntent() {
        return (String) getIntent().getSerializableExtra(ROOM);
    }

    private void initGoogleSignInClient() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
    }




    private void setMessageToMessageTextView(User user) {
        String message = "You joined roomID: " + roomID;
        binding.tvLoggedUser.setText(message);
    }
}
