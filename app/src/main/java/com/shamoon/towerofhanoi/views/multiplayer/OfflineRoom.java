package com.shamoon.towerofhanoi.views.multiplayer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.shamoon.towerofhanoi.R;
import com.shamoon.towerofhanoi.databinding.ActivityOfflineRoomBinding;
import com.shamoon.towerofhanoi.views.game.Play;

public class OfflineRoom extends AppCompatActivity {
    ActivityOfflineRoomBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_offline_room);
    }

    @Override
    public void onBackPressed() { }

    public void goBack(View view){
        this.finish();
    }

    public void playGame(View view){
        if (binding.editTextName1.getText().toString().trim().equals("")){
            Toast.makeText(OfflineRoom.this, "Enter first player name", Toast.LENGTH_SHORT).show();
        }else if (binding.editTextName2.getText().toString().trim().equals("")){
            Toast.makeText(OfflineRoom.this, "Enter second player name", Toast.LENGTH_SHORT).show();
        }else {
            Intent intent = new Intent(OfflineRoom.this, Play.class);
            intent.putExtra("player1", binding.editTextName1.getText().toString());
            intent.putExtra("player2", binding.editTextName2.getText().toString());
            intent.putExtra("numofdisks", 3);
            startActivity(intent);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
}
