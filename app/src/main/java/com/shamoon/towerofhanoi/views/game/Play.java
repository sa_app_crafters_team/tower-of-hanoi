package com.shamoon.towerofhanoi.views.game;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.View;

import com.shamoon.towerofhanoi.R;
import com.shamoon.towerofhanoi.databinding.ActivityPlayBinding;
import com.shamoon.towerofhanoi.model.Mode;
import com.shamoon.towerofhanoi.model.Result;
import com.shamoon.towerofhanoi.model.User;
import com.shamoon.towerofhanoi.model.UserResult;
import com.shamoon.towerofhanoi.views.custom.Draw;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Play extends AppCompatActivity {
    UserResult player1, player2;
    int currentPlayer, width, minPossibleMoves;
    Draw gameView;
    float height;
    CountDownTimer countDownTimer;
    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    Handler handler;
    int Seconds, Minutes, MilliSeconds ;
    ActivityPlayBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_play);

        init();
    }

    private void init() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

        player1 = new UserResult(new User(getIntent().getStringExtra("player1")));
        player2 = new UserResult(new User(getIntent().getStringExtra("player2")));

        minPossibleMoves = new BigDecimal(2).pow(
                getIntent().getExtras().getInt("numofdisks")).intValue() - 1;

        currentPlayer = 1;

        handler = new Handler();

        height = (float) (displaymetrics.heightPixels - (displaymetrics.heightPixels * 0.1));
        width = displaymetrics.widthPixels;

        binding.tvPlayerName.setText(player1.getUser().name);

        gameView = new Draw(this, width, height, 3);
        binding.dArea.addView(gameView);

        gameView.setTouchEnable(false);

        countDownTimer = new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
                gameView.setTouchEnable(false);
                binding.tvCountdown.setText("Seconds: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                gameView.setTouchEnable(true);
                binding.tvCountdown.setText("Seconds: 0");
                StartTime = SystemClock.uptimeMillis();
                handler.postDelayed(runnable, 0);
            }
        };

        countDownTimer.start();
    }

    public void gameOver(int moves) {
        countDownTimer.cancel();

        if (currentPlayer == 1){
            currentPlayer = 2;
            binding.tvPlayerName.setText(player2.getUser().name);
            binding.dArea.removeAllViews();
            gameView = new Draw(this, width, height, 3);
            binding.dArea.addView(gameView);
            player1.setMovesTaken(moves);
            countDownTimer.start();
            player1.setTimeTaken(MillisecondTime);
            pauseTimer();
            resetTimer();
        }else {
            currentPlayer = 0;
            player2.setMovesTaken(moves);
            player2.setTimeTaken(MillisecondTime);
            showDialog();
            pauseTimer();
            resetTimer();
        }
    }

    public void showDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Leader Board - Game Over");

        List<UserResult> userResults = new ArrayList<>();
        userResults.add(player1);
        userResults.add(player2);

        Result result = new Result(userResults, null, Mode.OFFLINE);

        UserResult winner = result.getFirstUserOfflineMode(minPossibleMoves);
        UserResult looser = null;
        if (winner.getUser().name.equals(player1.getUser().name))
            looser = player2;
        else looser = player1;

        alert.setMessage(
                winner.getUser().name + " you won, congrats\n" +
                        looser.getUser().name + "you failed, try again"
        );

        /*if (player1.getMovesTaken() > player2.getMovesTaken())
            alert.setMessage(player2.getUser().name + " you won, congrats!!!!");
        else
            alert.setMessage(player1.getUser().name + " you won, congrats!!!!");*/

        alert.setPositiveButton("Back", (dialog, whichButton) -> finish());

        alert.create().show();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void pauseTimer(){
        TimeBuff += MillisecondTime;
        handler.removeCallbacks(runnable);
    }

    private void resetTimer(){
        MillisecondTime = 0L ;
        StartTime = 0L ;
        TimeBuff = 0L ;
        UpdateTime = 0L ;
        Seconds = 0 ;
        Minutes = 0 ;
        MilliSeconds = 0 ;
        binding.tvTimer.setText("00:00:00");
    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);

            binding.tvTimer.setText("" + Minutes + ":"
                    + String.format("%02d", Seconds) + ":"
                    + String.format("%03d", MilliSeconds));

            handler.postDelayed(this, 0);
        }
    };
}
