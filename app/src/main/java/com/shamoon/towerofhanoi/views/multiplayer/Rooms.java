package com.shamoon.towerofhanoi.views.multiplayer;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.shamoon.towerofhanoi.R;
import com.shamoon.towerofhanoi.databinding.ActivityRoomsBinding;
import com.shamoon.towerofhanoi.model.User;
import com.shamoon.towerofhanoi.views.MainActivity;
import com.shamoon.towerofhanoi.views.login.LoginOrSignup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.shamoon.towerofhanoi.model.Constants.ROOM;
import static com.shamoon.towerofhanoi.model.Constants.USER;

public class Rooms extends AppCompatActivity {

    GoogleSignInClient googleSignInClient;
    ActivityRoomsBinding binding;
    FirebaseFirestore firestore;
    User currentUser;
    List<DocumentSnapshot> documents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rooms);
        init();
    }

    private void init() {
        currentUser = getUserFromIntent();
        initGoogleSignInClient();
        setMessageToMessageTextView(currentUser);
        documents = new ArrayList<>();

        firestore = FirebaseFirestore.getInstance();
        binding.btnJoinRoom.setEnabled(false);

        firestore.collection("rooms").get().addOnSuccessListener(queryDocumentSnapshots -> {
            documents.clear();
            documents.addAll(queryDocumentSnapshots.getDocuments());
            //List<DocumentSnapshot> documents = queryDocumentSnapshots.getDocuments();
            for (int i = 0; i < documents.size(); i++){
                DocumentSnapshot room = documents.get(i);
                RadioButton button = new RadioButton(this);
                button.setId(i);
                button.setText(room.get("Room").toString());

                binding.radioGroupRooms.addView(button);
            }
            binding.btnJoinRoom.setEnabled(true);
        });


        binding.btnJoinRoom.setOnClickListener(v->{
            int roomIndex = binding.radioGroupRooms.getCheckedRadioButtonId();
            DocumentSnapshot documentSnapshot = documents.get(roomIndex);
            goToPlayScreen(documentSnapshot);
        });

        binding.btnLogout.setOnClickListener(view -> {
            googleSignInClient.signOut().addOnCompleteListener(this, task -> {
                startActivity(new Intent(Rooms.this, LoginOrSignup.class));
            });
        });

        binding.btnCreateRoom.setOnClickListener(view -> {
            final Dialog dialog = new Dialog(Rooms.this);
            dialog.setContentView(R.layout.dialog_create_room);
            EditText etRoomName = dialog.findViewById(R.id.edtRoomName);
            dialog.findViewById(R.id.btnCancel).setOnClickListener(v -> {
                dialog.dismiss();
            });

            dialog.findViewById(R.id.btnOk).setOnClickListener(v -> {
                String roomName = etRoomName.getText().toString().trim();
                Map<String, Object> room = new HashMap<>();
                room.put("Room", roomName);
                firestore.collection("rooms")
                        .add(room)
                        .addOnSuccessListener(documentReference -> {
                            Toast.makeText(Rooms.this, "Room Created: " + roomName, Toast.LENGTH_SHORT).show();
                            goToPlayScreen(documentReference);
                            dialog.dismiss();
                        })
                        .addOnFailureListener(e -> {
                            Toast.makeText(Rooms.this, "Room not created", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        });

            });
            dialog.show();
        });
    }

    private void goToPlayScreen(DocumentReference documentReference) {
        Intent intent = new Intent(Rooms.this, MainActivity.class);
        intent.putExtra(USER, currentUser);
        intent.putExtra(ROOM, documentReference.getId());
        startActivity(intent);
    }

    private void goToPlayScreen(DocumentSnapshot documentSnapshot) {
        Intent intent = new Intent(Rooms.this, MainActivity.class);
        intent.putExtra(USER, currentUser);
        intent.putExtra(ROOM, documentSnapshot.getId());
        startActivity(intent);
    }

    private User getUserFromIntent() {
        return (User) getIntent().getSerializableExtra(USER);
    }

    private void initGoogleSignInClient() {
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
    }


    private void setMessageToMessageTextView(User user) {
        String message = "You are logged in as: " + user.name;
        binding.tvLoggedUser.setText(message);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }
}

