package com.shamoon.towerofhanoi.model;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public class Result {
    List<UserResult> results;
    private boolean isCurrentUserFirst;
    User currentUser;

    public Result(){
        results = new ArrayList<>();
    }
    public Result(List<UserResult> results, @Nullable User currentUser, Mode mode){
        this.results = results;
        if (mode.equals(Mode.ONLINE)){
            this.currentUser = currentUser;
            setCurrentUserFirstOnlineMode();
        }
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void setResultsOnlineMode(List<UserResult> results) {
        this.results = results;
        setCurrentUserFirstOnlineMode();
    }

    public void setResultsOfflineMode(List<UserResult> results) {
        this.results = results;
    }

    public boolean isCurrentUserFirst() {
        return isCurrentUserFirst;
    }

    public void setCurrentUserFirstOnlineMode() {
        if (results.size() <= 0){
            isCurrentUserFirst = false;
        } else {
            double minTime = results.get(0).getTimeTaken();
            int minMoves = results.get(0).getMovesTaken();
            User winner = results.get(0).getUser();
            for (UserResult res: results) {
                if (res.getMovesTaken() < minMoves && res.getTimeTaken() < minTime){
                    minMoves = res.getMovesTaken();
                    minTime = res.getTimeTaken();
                    winner = res.getUser();
                }
            }
            if (winner.uid.equals(currentUser.uid)){
                isCurrentUserFirst = true;
            }
        }
    }

    public UserResult getFirstUserOfflineMode(int minPossibleMoves) {
        UserResult winner = null;
        if (results.size() > 0){
            double minTime = results.get(0).getTimeTaken();
            int minMoves = results.get(0).getMovesTaken();
            winner = results.get(0);
            for (UserResult res: results) {
                if ((res.getMovesTaken() < minMoves || res.getMovesTaken() == minPossibleMoves) && res.getTimeTaken() < minTime){
                    minMoves = res.getMovesTaken();
                    minTime = res.getTimeTaken();
                    winner = res;
                }
            }
        }
        return winner;
    }
}
