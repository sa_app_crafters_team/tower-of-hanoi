package com.shamoon.towerofhanoi.model;

public class UserResult {
    private User user;
    private double timeTaken;
    private int movesTaken;

    public UserResult(){}

    public UserResult(User user, double timeTaken, int movesTaken) {
        this.user = user;
        this.timeTaken = timeTaken;
        this.movesTaken = movesTaken;
    }
    public UserResult(User user) {
        this.user = user;
        this.timeTaken = 0;
        this.movesTaken = 0;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(double timeTaken) {
        this.timeTaken = timeTaken;
    }

    public int getMovesTaken() {
        return movesTaken;
    }

    public void setMovesTaken(int movesTaken) {
        this.movesTaken = movesTaken;
    }
}
